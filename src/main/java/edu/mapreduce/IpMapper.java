package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.counters.InvalidIpRowsCounter;
import edu.mapreduce.counters.InvalidSeparatorRowsCounter;
import edu.mapreduce.dto.ValueDto;
import edu.mapreduce.utils.IpAddressValidator;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Строки вида
 * 0.0.0.0 - - [20/Sep/2010:13:02:42  +0100] "DELETE /kmqjnm/vfedvj/ofwwixesxlb/bgymj HTTP/1.1" 404 3856 "zuc" "tnfxniprdzokqadbmqxnfkknemobyqvfofod"
 * преобразуются в пары вида <Text,Text>
 * 0.0.0.0 -  {"requestCount":1,"sumLength":134,"avgLength":134.0}
 * где
 *      requestCount количество запросов на данный ip
 *      sumLength - суммарная длина всех запросов на данный ip
 *      avgLength - средняя длина запросов на данный ip
 */
public class IpMapper extends Mapper<Object, Text, Text, Text> {

    /**
     * Разделитель между ip и логом
     */
    private static final String SEPARATOR = " - - ";

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        // Перевод строки из файла в String
        String line = value.toString();
        // Найти в строке разделитель ip и лога, если он есть
        int ipAddressEnd = line.indexOf(SEPARATOR);
        // Если разделителя нет, увеличить соответствующий счетчик
        if (ipAddressEnd == -1) {
            context.getCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR).increment(1);
            return;
        }
        // Выделить ip адрес из строки
        String ipAddressString = line.substring(0, ipAddressEnd);
        // Проверить ip на валидность
        IpAddressValidator validator = new IpAddressValidator();
        // Если у ip неправильный формат, увеличить соответствующий счетчик
        if (!validator.isValid(ipAddressString)) {
            context.getCounter(InvalidIpRowsCounter.INVALID_IP).increment(1);
            return;
        }
        // Выделить информацию о запросе из лога
        String content = line.substring(ipAddressEnd + SEPARATOR.length());
        // Создать json с информацией <количество запроосов для ip (в мапере всегда 1), длина информации, длина информации>
        Gson gson = new Gson();
        ValueDto valueDto = new ValueDto(1, content.length(), content.length());
        String json = gson.toJson(valueDto);
        // Передать пару <ip,статистика>
        context.write(new Text(ipAddressString), new Text(json));
    }
}
