package edu.mapreduce;

import com.google.gson.Gson;
import edu.mapreduce.counters.InvalidIpRowsCounter;
import edu.mapreduce.counters.InvalidSeparatorRowsCounter;
import edu.mapreduce.dto.ValueDto;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.junit.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Тесты запускаются при билде
 */
public class IpLogsMapReduceTest {

    private IpMapper mapper;
    private IpReducer reducer;
    private MapDriver<Object, Text, Text, Text> mapDriver;
    private ReduceDriver<Text, Text, Text, Text> reduceDriver;
    private MapReduceDriver<Object, Text, Text, Text, Text, Text> mapReduceDriver;

    @Before
    public void setup() {
        mapper = new IpMapper();
        reducer = new IpReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer, reducer);
    }

    /**
     * Тест mapper с корректными данными
     *
     * @throws IOException
     */
    @Test
    public void mapperGoodTest() throws IOException {
        // Формирование строки
        String ipAddress = "0.0.0.0";
        String content = "[20/Sep/2010:13:02:42  +0100] \"DELETE /kmqjnm/vfedvj/ofwwixesxlb/bgymj HTTP/1.1\" 404 3856 \"zuc\" \"tnfxniprdzokqadbmqxnfkknemobyqvfofod\"";
        Text text1 = new Text(String.format("%s - - %s", ipAddress, content));
        // Задание входных параметров mapper
        mapDriver.withInput(new IntWritable(1), text1);
        // Рассчет ожидаемого результата
        Gson gson = new Gson();
        ValueDto dto = new ValueDto(1, content.length(), content.length());
        String json = gson.toJson(dto);
        // Задание ожидаемого результата
        mapDriver.withOutput(new Pair<Text, Text>(new Text("0.0.0.0"), new Text(json)));
        // Запуск теста
        mapDriver.runTest();
        // проверка счетчиков
        Counter invalidIpCounter = mapDriver.getCounters().findCounter(InvalidIpRowsCounter.INVALID_IP);
        Counter invalidSeparatorCounter = mapDriver.getCounters().findCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR);
        Assert.assertEquals(0, invalidIpCounter.getValue());
        Assert.assertEquals(0, invalidSeparatorCounter.getValue());
    }

    /**
     * Тест с некорректным ip
     *
     * @throws IOException
     */
    @Test
    public void mapperBadIpTest() throws IOException {
        // Формирование строки
        String ipAddress = "0.0.0.1230";
        String content = "[20/Sep/2010:13:02:42  +0100] \"DELETE /kmqjnm/vfedvj/ofwwixesxlb/bgymj HTTP/1.1\" 404 3856 \"zuc\" \"tnfxniprdzokqadbmqxnfkknemobyqvfofod\"";
        Text text1 = new Text(String.format("%s - - %s", ipAddress, content));
        // Задание входных параметров mapper
        mapDriver.withInput(new IntWritable(1), text1);
        // Запуск теста
        mapDriver.runTest();
        // проверка счетчиков
        Counter invalidIpCounter = mapDriver.getCounters().findCounter(InvalidIpRowsCounter.INVALID_IP);
        Counter invalidSeparatorCounter = mapDriver.getCounters().findCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR);
        Assert.assertEquals(1, invalidIpCounter.getValue());
        Assert.assertEquals(0, invalidSeparatorCounter.getValue());
    }

    /**
     * Тест с некорректным разделителем
     *
     * @throws IOException
     */
    @Test
    public void mapperBadSeparatorTest() throws IOException {
        // Формирование строки
        String ipAddress = "0.0.0.1230";
        String content = "[20/Sep/2010:13:02:42  +0100] \"DELETE /kmqjnm/vfedvj/ofwwixesxlb/bgymj HTTP/1.1\" 404 3856 \"zuc\" \"tnfxniprdzokqadbmqxnfkknemobyqvfofod\"";
        Text text1 = new Text(String.format("%s -- %s", ipAddress, content));
        // Задание входных параметров mapper
        mapDriver.withInput(new IntWritable(1), text1);
        // Запуск теста
        mapDriver.runTest();
        // проверка счетчиков
        Counter invalidIpCounter = mapDriver.getCounters().findCounter(InvalidIpRowsCounter.INVALID_IP);
        Counter invalidSeparatorCounter = mapDriver.getCounters().findCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR);
        Assert.assertEquals(0, invalidIpCounter.getValue());
        Assert.assertEquals(1, invalidSeparatorCounter.getValue());
    }

    /**
     * Тест данные на входе корректны
     *
     * @throws IOException
     */
    @Test
    public void testReducer() throws IOException {
        // Создание входных данных
        Gson gson = new Gson();
        Text ipAddress = new Text("0.0.0.0");

        ValueDto dto1 = new ValueDto(10, 1000, 100);
        ValueDto dto2 = new ValueDto(6, 120, 20);
        ValueDto dto3 = new ValueDto(4, 40, 20);

        List<Text> values = Arrays.asList(new Text(gson.toJson(dto1)), new Text(gson.toJson(dto2)), new Text(gson.toJson(dto3)));
        // Создание выходных данных
        ValueDto answerDto = new ValueDto(20, 1160, 58);
        String answerJson = gson.toJson(answerDto);
        // Задание входных параметров reducer
        reduceDriver.withInput(ipAddress, values);
        // Задание ожидаемого результата
        reduceDriver.withAllOutput(Collections.singletonList(
                new Pair<>(ipAddress, new Text(answerJson))
        ));
        // Запуск теста
        reduceDriver.runTest();
    }

    /**
     * На входе как корректные данные так и нет
     *
     * @throws IOException
     */
    @Test
    public void testMapReduce() throws IOException {
        // Создание входных данных
        List<String> ipAddressList = Arrays.asList("0.0.0.0", "195.168.0.1", "0.0.0.0", "asdal;s", "0.0.0.0111", "0.0.0.0");
        List<String> contentList = Arrays.asList(
                "[20/Sep/2010:13:02:42  +0100] \"DELETE /kmqjnm/vfedvj/ofwwixesxlb/bgymj HTTP/1.1\" 404 3856 \"zuc\" \"tnfxniprdzokqadbmqxnfkknemobyqvfofod\"",
                "[20/Sep/2010:13:05:17  +0100] \"POST /c HTTP/1.0\" 201 4412 \"tcd\" \"uqqgmspicumrsafgcjikxzmjabctptvfvhwuocryawobasq\"",
                "[20/Sep/2010:13:14:46  +0100] \"DELETE /pqeyf/bvmbwfxnq/jgzcqwcfsr/xzrttvffhm HTTP/1.1\" 405 7552 \"zikknfsmdfyauwktjeplwgmvqpaxutx\" \"cednwwuwjtozpdwjfselvy\"",
                "[20/Sep/2010:13:46:00  +0100] \"GET /guiejhouh HTTP/1.1\" 500 1838 \"cumgjgxzjygmnelcbuh\" \"spmfwkooaywzsjbtxvkuebm\"",
                "[20/Sep/2010:13:46:00  +0100] \"GET /guiejhouh HTTP/1.1\" 500 1838 \"cumgjgxzjygmnelcbuh\" \"spmfwkooaywzsjbtxvkuebm\"",
                "[20/Sep/2010:13:46:00  +0100] \"GET /guiejhouh HTTP/1.1\" 500 1838 \"cumgjgxzjygmnelcbuh\" \"spmfwkooaywzsjbtxvkuebm\""
        );

        List<Text> textList = new ArrayList<>();
        for (int i = 0; i < ipAddressList.size() - 1; i++) {
            textList.add(new Text(String.format("%s - - %s", ipAddressList.get(i), contentList.get(i))));
        }

        textList.add(new Text(String.format("%s -- %s", ipAddressList.get(ipAddressList.size() - 1), contentList.get(ipAddressList.size() - 1))));
        // Задание входных параметров map reducer
        for (Text text : textList) {
            mapReduceDriver.withInput(new IntWritable(1), text);
        }
        // Запуск теста
        final List<Pair<Text, Text>> result = mapReduceDriver.run();
        // Проверка количества возвращаемых значений
        Assert.assertEquals(2, result.size());
        // Задание ожидаемого резу
        Gson gson = new Gson();
        ValueDto dto1 = new ValueDto(
                2,
                contentList.get(0).length() + contentList.get(2).length(),
                ((double) (contentList.get(0).length() + contentList.get(2).length())) / 2);
        ValueDto dto2 = new ValueDto(
                1,
                contentList.get(1).length(),
                contentList.get(1).length());
        // проверка счетчиков
        Counter invalidIpCounter = mapReduceDriver.getCounters().findCounter(InvalidIpRowsCounter.INVALID_IP);
        Counter invalidSeparatorCounter = mapReduceDriver.getCounters().findCounter(InvalidSeparatorRowsCounter.INVALID_SEPARATOR);

        Assert.assertTrue(result.contains(new Pair<Text, Text>(
                new Text(ipAddressList.get(0)), new Text(gson.toJson(dto1)))));
        Assert.assertTrue(result.contains(new Pair<Text, Text>(
                new Text(ipAddressList.get(1)), new Text(gson.toJson(dto2)))));

        Assert.assertEquals(2, invalidIpCounter.getValue());
        Assert.assertEquals(1, invalidSeparatorCounter.getValue());
    }
}
