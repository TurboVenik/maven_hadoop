import datetime
import pprint
import random
import string
import time

import numpy

# количество генерируемых строк
raws_count = 400000
# запиись статистики
result_map = {
    "ip_count": {},
    "bad_ip": 0,
    "bad_separator": 0
}

# данные полезные для генерации логов
current_date = datetime.datetime(2010, 9, 20, 13, 00)
http_methods = ["GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"]
http_codes = [200, 201, 400, 401, 404, 405, 500]
random_ip_parts = [50, 114, 195, 67, 53, 123, 45, 89]
ip_pool = ["99.168.127.53", "67.195.114.50", "67.195.114.51", "67.195.114.52", "0.0.0.0"]


# свозвращает случайную строку случайной длины
def random_string(string_length=None):
    if string_length is None:
        string_length = random.randint(1, 255)
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))


# Возвращается неправильный ip
def generate_bad_ip():
    case = random.randint(0, 2)

    if case == 0:
        return f"{random.randint(0, 100)}.{random.randint(0, 100)}.{random.randint(0, 100)}"
    if case == 1:
        return f"{random_string()}.{random_string()}.{random_string()}.{random_string()}"
    if case == 2:
        return f"{random.randint(256, 1000)}.{random.randint(0, 1000)}.{random.randint(0, 255)}.{random.randint(2, 10)}"


# Возвращает правильный ip, если random_int != 0
def generate_ip(random_int):
    if random_int == 0:
        result_map["bad_ip"] += 1
        return generate_bad_ip()

    if random_int > 90:
        # генерируется ip из случайных заданных частей
        random_ip = f"{random_ip_parts[random.randint(0, len(random_ip_parts) - 1)]}.{random_ip_parts[random.randint(0, len(random_ip_parts) - 1)]}.{random_ip_parts[random.randint(0, len(random_ip_parts) - 1)]}.{random_ip_parts[random.randint(0, len(random_ip_parts) - 1)]}"
    else:
        # выбиратеся один из ip из списка
        random_ip = ip_pool[random.randint(0, len(ip_pool) - 1)]

    # если random_int == 1 значит был сгенерирован неправильный разделитель, значит не надо зачитывать ip в статистику
    if random_int != 1:
        if random_ip in result_map["ip_count"]:
            result_map["ip_count"][random_ip] += 1
        else:
            result_map["ip_count"][random_ip] = 1
    return random_ip


# генератор дат
def next_date():
    global current_date
    current_date = current_date + datetime.timedelta(seconds=random.randint(0, 50))
    return f"{current_date.strftime('%d/%b/%Y:%H:%M:%S ')} +0100"


# генератор путей
def generate_request_path():
    request_path = "/".join([random_string(random.randint(1, 12)) for _ in range(random.randint(1, 5))])
    return f'{http_methods[random.randint(0, len(http_methods) - 1)]} /{request_path}'


# генератор версий http протокола
def generate_http_version():
    return f"HTTP/1.{random.randint(0, 1)}"


# генератор кода ответа
def generate_response_code():
    return http_codes[random.randint(1, len(http_codes) - 1)]


# генератор случайных чисел
def generate_number():
    return random.randint(0, 10000)


# генератор неправильных разделителей
def generate_bad_separator():
    case = random.randint(0, 2)

    if case == 0:
        return f" --"
    if case == 1:
        return f" -  -"
    if case == 2:
        return f" -----{random_string()}"


# возвращает правильный разделитель если random_int != 1
def generate_separator(random_int):
    if random_int == 1:
        result_map["bad_separator"] += 1
        return generate_bad_separator()
    return " - - "


# заполнение файла
start_time = time.time()
with open("../hadoop/image/file", "wb") as file:
    chunk_len = 10000

    for i in range(int(raws_count / chunk_len)):
        raws = []
        for j in range(chunk_len):
            random_int = random.randint(0, 100)
            raw = f"{generate_ip(random_int)}{generate_separator(random_int)}[{next_date()}] \"{generate_request_path()} {generate_http_version()}\" {generate_response_code()} {generate_number()} \"{random_string()}\" \"{random_string()}\"\n"
            raws.append(raw.encode("utf-8"))
        file.writelines(raws)
# вывод статистики
print(time.time() - start_time)
pprint.PrettyPrinter()
pprint.pprint(result_map)
